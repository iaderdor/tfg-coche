masa = 1100;
leje = 2.51;
empate = 1.485;
altura = 1.510;
coefaero = 0.286;

maxpotencia = 73000/30;
torquemax = 125;
maxn = 6000/60*2*pi;
minn = 900/60*2*pi;
MD =  Motor('diesel',torquemax,maxpotencia,maxn,minn);
reltrans = [0.6, 0.6, 0.9631, 1.549, 2.489, 4];
efi = 0.98;
CC = CajaCambios(reltrans, efi);

radiorueda = 0.32;
densidadrueda = 600;

coche_diesel = Coche(masa, leje, empate, altura, coefaero, MD, CC, radiorueda, densidadrueda);


deltat = 0.01;
tfin = 100;

coche_diesel.embrague.pedal = 1;
coche_diesel.cajadecambios.cambiarMarcha(int8(5));
velocidad_die = [];
aceleracionp = [];
trac = [];
torq = [];
t = (0:tfin/deltat) * deltat;

for frame = 0:tfin/deltat
    frame * deltat
    if frame * deltat <= 1
       coche_diesel.embrague.pedal = 1 - frame * deltat;
    elseif 1 <= frame * deltat && frame * deltat <= 6
        %Acelera 1
        coche_diesel.motor.acelerador = 1/7 * ( frame * deltat - 1);
    elseif  6 < frame * deltat && frame * deltat <= 6.5
        coche_diesel.motor.acelerador = 0;
        coche_diesel.embrague.pedal = 2 * ( frame * deltat - 6);
    elseif 6.5 < frame * deltat && frame * deltat <= 7.5
        coche_diesel.cajadecambios.cambiarMarcha(int8(2))
    elseif 7.5 < frame * deltat && frame * deltat <= 8
        coche_diesel.embrague.pedal = 2 * ( 8 - frame * deltat);
    elseif 8 < frame * deltat && frame * deltat <= 15
        % Acelera 2
        coche_diesel.motor.acelerador = 1/7 * ( frame * deltat - 8);
    elseif  15 < frame * deltat && frame * deltat <= 15.5
        coche_diesel.motor.acelerador = 0;
        coche_diesel.embrague.pedal = 2 * ( frame * deltat - 15);
    elseif 15.5 < frame * deltat && frame * deltat <= 16.5
        coche_diesel.cajadecambios.cambiarMarcha(int8(3))
    elseif 16.5 < frame * deltat && frame * deltat <= 17
        coche_diesel.embrague.pedal = 2 * ( 17 - frame * deltat);
    elseif 17 < frame * deltat && frame * deltat <= 25
        % Acelera 3
        coche_diesel.motor.acelerador = 1/10 * ( frame * deltat - 17);
    end
    
    [potencia, torquemotor, nmotor, trrd, trrt, resrodadura, resaero, resgradiente, deslizamiento, aceleracion] = coche_diesel.simular(deltat);
    velocidad_die = [velocidad_die, norm(coche_diesel.velocidad3)];
    aceleracionp = [aceleracionp, aceleracion];
    trac = [trac, 2*trrd];
    torq = [torq, torquemotor];
        
end

